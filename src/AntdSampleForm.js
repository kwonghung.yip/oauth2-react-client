import React from 'react';
import { connect } from "react-redux";
import { Input, Button, DatePicker, Form, Icon } from 'antd';
import FormItem from 'antd/lib/form/FormItem';

const Search = Input.Search;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

class AntdSampleForm extends React.Component {
    constructor (props) {
        super(props);
    }

    onChange(date, dateString) {
        console.log(date, dateString);
    }

    onSubmit = (event) => {
        event.preventDefault();
    }

    render() {
        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched, getFieldProps } = this.props.form;

        return (
            <div>
                <Form layout="inline" onSubmit={this.onSubmit}>
                    <FormItem label="Username">
                        { getFieldDecorator('username',{
                            initialValue: "ancd234",
                            rules: [{
                                required: true
                            },{
                                min:4
                            },{
                                max: 10
                            }]                            
                            })(
                        <Input 
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} 
                            placeholder="Username" />}/>
                        )}
                    </FormItem>
                    <FormItem label="Password">
                        { getFieldDecorator('password')(
                        <Input type="password" 
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} 
                            placeholder="Password" />}/>
                        )}
                    </FormItem>
                    <FormItem>
                        <Button htmlType="button">Load</Button>
                    </FormItem>
                    <FormItem>
                        <Button htmlType="button">Save</Button>
                    </FormItem>
                </Form>
            </div>
        )
    }
}

const WrappedAntdSampleForm = connect(
    (state) => {
        console.log(state);
        return Object.assign({},state.user);
    }
)(Form.create({
    mapPropsToFields(props) {
        console.log(props);
        return {
            username: Form.createFormField({value:props.username}),
            password: Form.createFormField({value:props.password})
        }
    },
    onFieldsChange2(props, fields) {
        console.log(fields);
    }
})(AntdSampleForm));

export default WrappedAntdSampleForm;

//export default AntdSampleForm;
